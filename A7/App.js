import React, { useEffect } from "react";
import './index.css'
import { useState } from "react";
function App() {
  const [isStarted, setisStarted] = useState(false);
  const [ispaused, setispaused] = useState(false);
  const [isReset, setisReset] = useState(false);

  const [seconds, setSeconds] = useState(0);
  const [minutes, setMinutes] = useState(0);
  const [hours, setHours] = useState(0);
  

  const handleStart = () =>{
    if(isStarted===false){
      setisStarted(!isStarted)
      setisReset(!isReset)
    }
    else if(isStarted===true && ispaused===false)
      setispaused(!ispaused)
    else
      setispaused(!ispaused)
  }

  const handleReset = () => {
      setisReset(!isReset)
      setispaused(false)
      setisStarted(false)
      setSeconds(0)
      setHours(0)
      setMinutes(0)
  }

  useEffect(() => {
    let interval = null;
    if (isStarted && !ispaused) {
      interval = setInterval(() => {
        if (seconds === 59 && minutes === 59) {
          setHours(hours + 1);
          setMinutes(0);
          setSeconds(0);
        } else if (seconds === 59) {
          setMinutes(minutes + 1);
          setSeconds(0);
        } else {
          setSeconds(seconds + 1);
        }
      }, 1000);
    } else {
      clearInterval(interval);
    }
    return () => clearInterval(interval);
  }, [isStarted, ispaused, seconds, minutes, hours]);

  return (
    <div className="container">
      <div >
        <h1>React Stopwatch</h1>

        <div className="timer">
          <h1>{String("0" + hours).slice(-2)} : {String("0" + minutes).slice(-2)} : {String("0" + seconds).slice(-2)}</h1>
        </div>

        <div className="buttons">
          <button onClick={handleStart}> {isStarted? ispaused? 'Resume' : 'Pause' : 'Start'} </button>
          <button disabled={!isReset} onClick={handleReset}>Reset</button>
        </div>

      </div>
    </div>
  );
}

export default App;
